// mobile nav
const openNav = document.querySelector('.open');
const closeNav = document.querySelector('.close');
const toggle = document.querySelector('.nav-list');


openNav.addEventListener('click', (e) => {
    toggle.classList.add('active');
    e.preventDefault();
} );

closeNav.addEventListener('click', (e) => {
    toggle.classList.remove('active');
    e.preventDefault();
} );

// desktop nav
const openNavDesktop = document.querySelector('.open-desktop');
const toggleDesktop = document.querySelector('.nav-list-desktop');
const DesktopNav = document.querySelector('.desktop-nav');
const closeNavDesktop = document.querySelector('.close-desktop');

openNavDesktop.addEventListener('click', (e) => {
    toggleDesktop.classList.add('active-desktop');
    e.preventDefault();
} );
openNavDesktop.addEventListener('click', (e) => {
    DesktopNav.classList.add('active-desktop-icon');
    e.preventDefault();
} );
closeNavDesktop.addEventListener('click', (e) => {
    toggleDesktop.classList.remove('active-desktop');
    e.preventDefault();
} );
closeNavDesktop.addEventListener('click', (e) => {
    DesktopNav.classList.remove('active-desktop-icon');
    e.preventDefault();
} );

// showing section part
const navItem1 = document.querySelector('.item-1');
const navItem2 = document.querySelector('.item-2');
const navItem3 = document.querySelector('.item-3');
const navItem4 = document.querySelector('.item-4');
const sectionAbout = document.querySelector('#section-about');
const sectionResume = document.querySelector('#section-resume');
const sectionWorks = document.querySelector('#section-works');
const sectionContact = document.querySelector('#section-contact');

navItem1.addEventListener('click', (e) => {
    sectionAbout.classList.add('active');
    sectionAbout.classList.add('fadeIn');
    sectionAbout.classList.remove('hidden');
    sectionAbout.classList.remove('fadeOut');

    sectionResume.classList.add('hidden');
    sectionResume.classList.add('fadeOut')
    sectionResume.classList.remove('active');
    sectionResume.classList.remove('fadeIn');

    sectionWorks.classList.add('hidden');
    sectionWorks.classList.add('fadeOut');
    sectionWorks.classList.remove('active');
    sectionWorks.classList.remove('fadeIn');

    sectionContact.classList.add('hidden');
    sectionContact.classList.add('fadeOut');
    sectionContact.classList.remove('active');
    sectionContact.classList.remove('fadeIn');

    e.preventDefault();   
})

navItem2.addEventListener('click', (e) => {
    sectionResume.classList.add('active');
    sectionResume.classList.add('fadeIn');
    sectionResume.classList.remove('hidden');
    sectionResume.classList.remove('fadeOut');

    sectionAbout.classList.add('hidden');
    sectionAbout.classList.add('fadeOut');
    sectionAbout.classList.remove('active');
    sectionAbout.classList.remove('fadeIn');

    sectionWorks.classList.add('hidden');
    sectionWorks.classList.add('fadeOut');
    sectionWorks.classList.remove('active');
    sectionWorks.classList.remove('fadeIn');

    sectionContact.classList.add('hidden');
    sectionContact.classList.add('fadeOut');
    sectionContact.classList.remove('active');
    sectionContact.classList.remove('fadeIn');

    e.preventDefault();
})

navItem3.addEventListener('click', (e) => {
    sectionWorks.classList.add('active');
    sectionWorks.classList.add('fadeIn');
    sectionWorks.classList.remove('hidden');
    sectionWorks.classList.remove('fadeOut');

    sectionAbout.classList.add('hidden');
    sectionAbout.classList.add('fadeOut');
    sectionAbout.classList.remove('active');
    sectionAbout.classList.remove('fadeIn');

    sectionResume.classList.add('hidden');
    sectionResume.classList.add('fadeOut')
    sectionResume.classList.remove('active');
    sectionResume.classList.remove('fadeIn');

    sectionContact.classList.add('hidden');
    sectionContact.classList.add('fadeOut');
    sectionContact.classList.remove('active');
    sectionContact.classList.remove('fadeIn');

    e.preventDefault();
})

navItem4.addEventListener('click', (e) => {
    sectionContact.classList.add('active');
    sectionContact.classList.add('fadeIn');
    sectionContact.classList.remove('hidden');
    sectionContact.classList.remove('fadeOut');

    sectionAbout.classList.add('hidden');
    sectionAbout.classList.add('fadeOut');
    sectionAbout.classList.remove('active');
    sectionAbout.classList.remove('fadeIn');

    sectionResume.classList.add('hidden');
    sectionResume.classList.add('fadeOut')
    sectionResume.classList.remove('active');
    sectionResume.classList.remove('fadeIn');

    sectionWorks.classList.add('hidden');
    sectionWorks.classList.add('fadeOut');
    sectionWorks.classList.remove('active');
    sectionWorks.classList.remove('fadeIn');

    e.preventDefault();
});

// text anim typewriter 
var app = document.querySelector('.welcome_text');

var typewriter = new typewriter(app, {
    loop: true
});

typewriter.typeString('Hello World!')
    .pauseFor(2500)
    .deleteAll()
    .typeString('Strings can be removed')
    .pauseFor(2500)
    .deleteChars(7)
    .typeString('<strong>altered!</strong>')
    .pauseFor(2500)
    .start();
